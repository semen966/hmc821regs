﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMC821Regs
{
    public class HMC821
    {
        /// <summary>Частота опоры, МГц (В метрологии 100)</summary>
        public double F_REF { get; private set; } = 100; 
        /// <summary>Делитель опоры (В Метрологии 2)</summary>
        public int K { get; private set; } = 2;                
        /// <summary>Частота гетеродина, МГц (в метрологии - половина требуемой выходной частоты F_OUT = 3468.5 МГц)</summary>
        public double Fvco { get; private set;} = 1734.25;       
        /// <summary>Регистры для записи в синтезатор (адрес/значение).
        /// Примечание - записывать строго по порядку, проверяя 10й регистр синтезатора
        /// на Busy (Недопустимо, когда Reg 10h[8] = 1)
        ///</summary>
        public List<Register> Registers { get; private set; } = new List<Register>();
        public HMC821()
        {
            Refrash();
        }

        public HMC821(double fref, int k, double fvco)
        {
            F_REF = fref;
            K = k;
            Fvco = fvco;
            Refrash();
        }
        void Refrash()
        {
            Registers.Clear();

            Registers.Add(new Register(2, K));//делитель опоры K=2   
            Registers.Add(new Register(6, 0x200B4A));
            Registers.Add(new Register(9, 0x403264));
            Registers.Add(new Register(5, 0xE80D));
            Registers.Add(new Register(5, 0x8395));
            Registers.Add(new Register(5, 0xD11D));
            Registers.Add(new Register(5, 0x5));

            //fvco = (fref/R)  * (Nint + Nfrac) = fint + ffrac
            //F_OUT = fvco / K;

            //fvco = F_OUT / K;
            double N = Fvco / F_REF;
            if (N < 20) N *= 2; //Должно выполняться 20<N<524284

            int Nint = (int)N;
            int Nfrac = (int)Math.Ceiling((N - Nint) * Math.Pow(2, 24));
            Registers.Add(new Register(3, Nint));//fvco /fref = 34.685=N
            Registers.Add(new Register(4, Nfrac));//Nfrac =0.685*224 (16777216)≈11492393
        }
    }
    public class Register
    {
        public int Address { get; set; }
        public int Value { get; set; }
        public Register() { }
        public Register(int addr, int value)
        {
            Address = addr;
            Value = value;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}=0x{1:x8},","Adress", Address);
            sb.AppendFormat("{0}=0x{1:x8}", "Value", Value);
            return sb.ToString();
        }
    }
}
